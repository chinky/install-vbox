#!/bin/bash
###########################################################################################
### Variables
LOGFILE="install.log"
###########################################################################################
### Functions
f_aborted() {
	whiptail --title "Script cancelled" --backtitle "${BACKTITLE}" --msgbox "         Aborted!" 7 32
	exit 0
}

# simple log-output
f_log() {
	echo "$1" >> $LOGFILE
	echo "$1"
}

###########################################################################################
### Begin of script
#set -e

f_log "Install Oracle_VM_VirtualBox_Extension_Pack..."
version=$(VBoxManage --version)
IFS='r' read -a versions <<< "${version}"
shasums_url=https://www.virtualbox.org/download/hashes/${versions[0]}
extpack=Oracle_VM_VirtualBox_Extension_Pack-${versions[0]}-${versions[1]}.vbox-extpack
extpack_url=http://download.virtualbox.org/virtualbox/${versions[0]}
wget -O ${extpack} ${extpack_url}/${extpack}
wget -O - ${shasums_url}/SHA256SUMS | grep ${extpack} | sha256sum -c
VBoxManage extpack install --replace ${extpack} <<- EOF
y
EOF

