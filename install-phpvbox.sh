#!/bin/bash
###########################################################################################
## 安装phpvbox
### Variables
LOGFILE="install.log"
phpvbox_url=" https://github.com/phpvirtualbox/phpvirtualbox/archive/master.zip"
phpvbox_file="phpvirtualbox.zip"
phpvbox_webpath="/srv/dev-disk-by-label-hd2/web/html"
phpvbox_rootpath="$phpvbox_webpath/vbox"
phpvbox_branch="phpvirtualbox-master"
phpvbox_user="vbox"
phpvbox_password="lldlcj123"
###########################################################################################
### Functions
f_aborted() {
	whiptail --title "Script cancelled" --backtitle "${BACKTITLE}" --msgbox "         Aborted!" 7 32
	exit 0
}

# simple log-output
f_log() {
	echo "$1" >> $LOGFILE
	echo "$1"
}

#
setphpvboxconfig(){
  sed -i "s/^\s*#*\s*var\s\+\$$(echo $1)\s\+=.*;/var \$$(echo $1) = $(echo $2);/g" $phpvbox_rootpath/config.php
}
###########################################################################################
### Begin of script
#set -ex

f_log "Install phpvirtualbox"
cat > /etc/default/virtualbox <<- EOF
# virtualbox defaults file
VBOXAUTOSTART_DB=/etc/vbox
VBOXAUTOSTART_CONFIG=/etc/vbox/autostart.cfg
VBOXWEB_USER=${phpvbox_user}
EOF

mkdir -vp /etc/vbox
cat > /etc/vbox/autostart.cfg <<- EOF
# Default policy is to deny starting a VM, the other option is "allow".
default_policy = allow

# Bob (userid) is allowed to start virtual machines but starting them
# will be delayed for 10 seconds
#bob = {
#    allow = true
#    startup_delay = 10
#}

# Alice is not allowed to start virtual machines, useful to exclude certain users
# if the default policy is set to allow.
#alice = {
#    allow = false
#}
EOF

chgrp vboxusers /etc/vbox
chmod 1775 /etc/vbox

wget -O $phpvbox_file $phpvbox_url

if !(command -v 7z >/dev/null 2>&1; )  then 
  apt-get -y --reinstall install p7zip-full 
fi

7z x -y -o$phpvbox_webpath $phpvbox_file
chown www-data:www-data $phpvbox_webpath/$phpvbox_branch -R
chmod 777 $phpvbox_webpath/$phpvbox_branch -R

if [ -d $phpvbox_rootpath ]; then
  mv -f $phpvbox_rootpath $phpvbox_rootpath.bak$(date +%Y%m%d%H%M%S)
fi
mv -f $phpvbox_webpath/$phpvbox_branch $phpvbox_rootpath
cp -af $phpvbox_rootpath/config.php-example $phpvbox_rootpath/config.php
#修改username
setphpvboxconfig "username" "'$(echo $phpvbox_user)'"
#修改pass
setphpvboxconfig "password" "'$(echo $phpvbox_password)'"
#Display guest additions version of a running VM on its Details tab
setphpvboxconfig "enableGuestAdditionsVersionDisplay"  "true"
#/* Enable advanced configuration items (normally hidden in the VirtualBox GUI)
# * Note that some of these items may not be translated to languages other than English.
# */
setphpvboxconfig "enableAdvancedConfig" "true"

#cp -f $phpvbox_rootpath/vboxinit /etc/init.d
#chmod u+rx /etc/init.d/vboxinit
#update-rc.d vboxinit defaults
setphpvboxconfig "startStopConfig"  "true"

systemctl restart vboxdrv
systemctl restart vboxweb-service
systemctl restart vboxautostart-service

runuser -l  ${phpvbox_user} -c 'VBoxManage setproperty autostartdbpath /etc/vbox'

systemctl status vboxdrv
systemctl status vboxweb-service
systemctl status vboxautostart-service 