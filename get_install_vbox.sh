#!/bin/bash

#usage:
#rm -vf get_install_omv.sh &&wget https://gitee.com/chinky/install-omv/raw/master/get_install_vbox.sh &&/bin/bash get_install_vbox.sh

#set -e

if [ ! -f "/usr/bin/git" ]; then
  apt-get update -y
  apt-get -y install git 
fi

cd ~
rm -fr install-vbox
git clone https://gitee.com/chinky/install-vbox.git

cd install-vbox
#/bin/bash install_openmediavault.sh