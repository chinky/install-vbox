#!/bin/bash
###########################################################################################
### Variables
vbox_ver="5.2"
LOGFILE="install.log"
###########################################################################################
### Functions
f_aborted() {
	whiptail --title "Script cancelled" --backtitle "${BACKTITLE}" --msgbox "         Aborted!" 7 32
	exit 0
}

# simple log-output
f_log() {
	echo "$1" >> $LOGFILE
	echo "$1"
}

###########################################################################################
### Begin of script
#set -e

f_log "========================================="

if !(command -v lsb_release >/dev/null 2>&1; )  then 
  f_log "install lsb-release"
  apt-get update -y
  apt-get -y install lsb-release
fi

RELEASE="$(lsb_release -c -s)" 

f_log "add virtualbox sources & key"
cat > /etc/apt/sources.list.d/virtualbox.list <<- EOF
deb http://download.virtualbox.org/virtualbox/debian $RELEASE contrib
EOF
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -

apt-get -y update

systemctl status vboxdrv
if [ $? -eq 0 ]; then
	systemctl stop vboxdrv
fi
systemctl status vboxweb-service
if [ $? -eq 0 ]; then
	systemctl stop vboxweb-service
fi

f_log "Install linux-headers-$(uname -r) ..."
apt-get install --reinstall -y linux-headers-$(uname -r)
f_log "Install  build-essential virtualbox-$vbox_ver dkms..."
apt-get install --reinstall -y build-essential virtualbox-$vbox_ver dkms

usermod -u 2000 vbox
groupmod -g 2000 vboxusers

#insatll extpack
source ./install-extpack.sh

#install phpvirtualbox
source ./install-phpvbox.sh
